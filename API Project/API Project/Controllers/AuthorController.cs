﻿using System;
using System.Collections.Generic;
using System.Linq;
using API_Project.ResourceParameters;
using APIProjectDTO.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Repository.Models;
using Service.Interfaces;

namespace API_Project.Controllers
{
    [Route("api/Authors")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly ICourseAuthorService _service;
        private readonly IMapper _mapper;

        public AuthorController(ICourseAuthorService service, IMapper mapper)
        {
            _service = service ??
                throw new ArgumentNullException(nameof(service));

            _mapper = mapper ??
                throw new ArgumentNullException(nameof(service));
        }

        // o puedo especificar el GET aqui
        [HttpGet]
        public IActionResult GetAuthors([FromQuery] AuthorsResourceParameters authorResourceParameters)
        {
            var authorsFromService = _service.GetAllAuthors(authorResourceParameters);

            // Método 2 con AutoMapper
            if (authorsFromService != null)
                return Ok(_mapper.Map<IEnumerable<AuthorDTO>>(authorsFromService));
            else
                return NotFound();

            // Método 1 hacerlo en el Controller, menos eficiente
            /*
            var listAuthors = authorsFromService
                .Select(res => new AuthorDTO
                { 
                    Id = res.Id,
                    Name = $"{res.Firstname} {res.Lastname}",
                    Age = Service.Helpers.DateTimeFormat.GetAge(res.DateOfBirth),
                    MainCategory = res.MainCategory
                });

            
            if (listAuthors == null)
                return NotFound();
            else
                return Ok(listAuthors)
            */
        }


        [HttpGet("{id}", Name = "GetAuthor")]
        public IActionResult GetAuthor(int id)
        {
            var authorFromService = _service.GetAuthor(id);

            if (authorFromService == null)
                return NotFound();
            else
                //return Ok(authorFromService);
                return Ok(_mapper.Map<AuthorDTO>(authorFromService));
        }


        //[HttpPost]
        //public ActionResult<AuthorDTO> PostAuthor(AuthorPostDTO author)
        //{
        //    // PostDTO a Entidad 'Author'
        //    var authorEntity = _mapper.Map<Repository.Models.Author>(author);

        //    // Paso entidad (Author) y la guardo (aqui ya hay un nuevo author
        //    _service.AddAuthor(authorEntity);

        //    // Ahora lo mapeo al viewModel que tengo de Author (AuthorDTO)
        //    var authorToReturn = _mapper.Map<AuthorDTO>(authorEntity);

        //    authorToReturn.Age = Service.Helpers.DateTimeFormat.GetAge(authorEntity.DateOfBirth);

        //    // Devolver author que acabo de crear, utlizando el método Get: GetAuthor' y pasandole el nuevo Id del Author recien creado
        //    return CreatedAtRoute("GetAuthor", new { Id = authorToReturn.Id }, authorToReturn);
        //}

        // Añadiendo mediante POST una colección de authores
        [HttpPost]
        //[Route("api/Authors/addCollection")]
        public ActionResult<IEnumerable<AuthorDTO>> PostCollectionAuthor
            (IEnumerable<AuthorPostDTO> authorsCollection)
        {
            // Mapeo la colección recibída en el POST a una colección de Authors
            var authorEntities = _mapper.Map<IEnumerable<Author>>(authorsCollection);

            // Mediante el service voy añadiendo cada item 
            foreach(var author in authorEntities)
            {
                _service.AddAuthor(author);
            }

            return Ok();
        }
    }
}