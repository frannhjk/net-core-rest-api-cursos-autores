﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIProjectDTO.Models;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.Interfaces;

namespace API_Project.Controllers
{
    [Route("api/Authors/{authorId}/Courses")]
    [ApiController]
    public class CoursesController : ControllerBase
    {

        private readonly ICourseAuthorService _service;
        private readonly IMapper _mapper;

        public CoursesController(ICourseAuthorService service, IMapper mapper)
        {
            _service = service ??
                throw new ArgumentNullException(nameof(service));

            _mapper = mapper ??
                throw new ArgumentNullException(nameof(service));
        }

        [HttpGet]
        public IActionResult GetCoursesByAuthor(int authorId)
        {
            var author = _service.GetAuthor(authorId);

            if (author == null)
                return NotFound();

            var coursesFromService = _service.GetAllCourses(authorId);
            return Ok(_mapper.Map<IEnumerable<CourseDTO>>(coursesFromService));
        }

        [HttpGet("{courseId}", Name= "GetCourseByAuthor")]
        public IActionResult GetCourseByAuthor(int authorId, int courseId)
        {
            var author = _service.GetAuthor(authorId); // Id pasado por parámetro GET

            if (author == null)
            {
                return NotFound();
            }
            else
            {
                var courses = _service.GetCourse(authorId, courseId);
                if (courses != null)
                    return Ok(_mapper.Map<CourseDTO>(courses));
                else
                    return NotFound();
            }
        }


        [HttpPost]
        public ActionResult<CourseDTO> PostCourseToAuthor(int authorId, CoursePostDTO course)
        {
            // Check si el author existe
            var author = _service.GetAuthor(authorId);

            if (author == null)
                return NotFound();

            // Mapeo parámetro DTO CoursePostDTO a Entidad Course
            var courseEntity = _mapper.Map<Repository.Models.Course>(course);

            // Lo agrego
            _service.AddCourse(authorId, courseEntity);

            // Retorno el nuevo curso, mapeo Entidad a Course DTO
            var courseToReturn = _mapper.Map<CourseDTO>(courseEntity);

            // Retorno Curso By Author: El recien creado/asignado
            return CreatedAtRoute("GetCourseByAuthor",
                new { authorId = authorId, courseId = courseToReturn.courseId },
                courseToReturn);
        }
    }


    
}