﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Project.ProfileMapper
{
    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            // Mapeo Model al DTO
            CreateMap <Repository.Models.Course, APIProjectDTO.Models.CourseDTO>();


            // Mapeo DTO al Model
            CreateMap<APIProjectDTO.Models.CoursePostDTO, Repository.Models.Course>();
        }
    }
}
