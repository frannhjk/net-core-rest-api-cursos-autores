﻿using AutoMapper;
using Service.Helpers;

namespace API_Project.ProfileMapper
{
    public class AuthorProfile : Profile
    {

        public AuthorProfile()
        {

            // Mapeo Entidad -> Model(DTO)
            CreateMap<Repository.Models.Author, APIProjectDTO.Models.AuthorDTO>()
                .ForMember(
                    dest => dest.Name,
                    opt => opt.MapFrom(src => $"{src.Firstname} {src.Lastname}")
                //.ForMember(
                   // dest => dest.Age,
                    //opt => opt.MapFrom(src => src.DateOfBirth)
                );


            // Profile para POST method en AuthorController
            CreateMap<APIProjectDTO.Models.AuthorPostDTO, Repository.Models.Author>(); // From DTO to Entity
        }
    }
}
