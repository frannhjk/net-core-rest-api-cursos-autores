﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Repository.Models
{
    public class Author
    {
        [Key]
        public int Id { get; private set; }

        [Required]
        [MaxLength(40)]
        public string Firstname { get; set; }

        [Required]
        [MaxLength(40)]
        public string Lastname { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [MaxLength(40)]
        public string MainCategory { get; set; }

        public DateTime? DateOfDeath { get; set; }

        public IEnumerable<Course> Courses { get; set; }

        public bool Activo { get; set; }



        public Author(string _firstname)
        {
            Firstname = _firstname;
        }

        public Author()
        {
       
        }
    }
}
