﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Repository.Models
{
    public class Course
    {
        [Key]
        public int CourseId { get; private set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(1500)]
        public string Description { get; set; }    

        [ForeignKey("AuthorId")]
        public Author Author { get; private set; }

        public int AuthorId { get; set; }

        
    }
}
