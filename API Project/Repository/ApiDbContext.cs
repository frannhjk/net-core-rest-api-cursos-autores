﻿using Microsoft.EntityFrameworkCore;
using Repository.Models;
using System;
using System.Data.Entity;

namespace Repository
{
    public class ApiDbContext : Microsoft.EntityFrameworkCore.DbContext
    {

        public ApiDbContext(DbContextOptions<ApiDbContext> options)
            : base(options)
        {
        }

        public Microsoft.EntityFrameworkCore.DbSet<Author> Authors { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Course> Courses { get; set; }
    }
}
