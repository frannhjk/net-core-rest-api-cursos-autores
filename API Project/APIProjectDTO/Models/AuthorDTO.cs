﻿using Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace APIProjectDTO.Models
{
    public class AuthorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string MainCategory { get; set; }

        public IEnumerable<Course>? Courses { get; set; }
    }
}
