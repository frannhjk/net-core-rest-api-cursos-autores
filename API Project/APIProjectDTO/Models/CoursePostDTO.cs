﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIProjectDTO.Models
{
    public class CoursePostDTO
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
