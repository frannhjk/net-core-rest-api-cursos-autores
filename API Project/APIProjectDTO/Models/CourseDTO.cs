﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIProjectDTO.Models
{
    public class CourseDTO
    {
        public int courseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int AuthorId { get; set; }
    }
}
