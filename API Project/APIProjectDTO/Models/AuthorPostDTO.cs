﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIProjectDTO.Models
{
    public class AuthorPostDTO
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string MainCategory { get; set; }

        // Cuando creo un Author pueda crear cursos asociados a él
        public ICollection<CoursePostDTO>? Courses { get; set; }
            = new List<CoursePostDTO>();
    }
}
