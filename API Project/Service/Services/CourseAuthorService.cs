﻿using API_Project.ResourceParameters;
using DocuSign.eSign.Model;
using Repository;
using Repository.Models;
using Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Service.Services
{
    public class CourseAuthorService : ICourseAuthorService
    {
        private ApiDbContext _context;

        //CTR
        public CourseAuthorService(ApiDbContext context)
        {
            _context = context ?? throw new ArgumentException(nameof(context));
        }

        // Courses
        #region Courses
        public IEnumerable<Course> GetAllCourses(int authorId)
        {
            //return _context.Courses
            //    .Include(a => a.Author)
            //    .Where(a => a.AuthorId == authorId);

            return _context.Courses
                    .Where(c => c.AuthorId == authorId);
        }

        public Course GetCourse(int authorId, int courseId)
        {
            return _context.Courses
                .FirstOrDefault(c => c.CourseId == courseId && c.AuthorId == authorId);
        }

        public void AddCourse(int authorId, Course course)
        {
            course.AuthorId = authorId;
            _context.Courses.Add(course);

            _context.SaveChanges();
        }


        public void UpdateCourse(Course course)
        {
            _context.Courses.Update(course);
            _context.SaveChanges();
        }


        public void DeleteCourse(Course course)
        {
            _context.Courses.Remove(course);
            _context.SaveChanges();
        }
        #endregion


        //Authors
        #region Authors
        public IEnumerable<Author> GetAllAuthors()
        {
            return _context.Authors;
        }

        public Author GetAuthor(int id)
        {
            return _context.Authors
                .FirstOrDefault(au => au.Id == id);
        }

        public IEnumerable<Author> GetAuthorsByCourse(IEnumerable<int> authorIds)
        {
            return _context.Authors
                .Where(a => authorIds.Contains(a.Id))
                .OrderBy(a => a.Firstname)
                .OrderBy(a => a.Lastname);
        }


        public void AddAuthor(Author author)
        {
            _context.Authors.Add(author);
            _context.SaveChanges();
        }


        public void UpdateAuthor(Author author)
        {
            _context.Authors.Update(author);
            _context.SaveChanges();
        }


        public void UpdateJustFirstname(int authorId, string newName)
        {
            //var author = new Author { Firstname = newName };
            //_context.Authors.Attach(author).Property(x => x.Firstname).IsModified = true;
            //_context.SaveChanges();


            // TODO : probar esto, junto con todos los updates
            var author = _context.Authors.FirstOrDefault(a => a.Id == authorId);
            author.Firstname = newName;
            _context.SaveChanges();
        }


        public void DeleteAuthor(Author author)
        {
            _context.Authors.Remove(author);
        }

        public void DeleteAuthorLogic(int authorId)
        {
            var author = _context.Authors.FirstOrDefault(a => a.Id == authorId);
            author.Activo = false;

            _context.SaveChanges();
        }

        public bool AuthorExists(int authorId)
        {
            return _context.Authors.Any(a => a.Id == authorId);
        }

        public IEnumerable<Author> GetAllAuthors(AuthorsResourceParameters authorResourceParameters)
        {
            if (authorResourceParameters == null)
                throw new ArgumentNullException(nameof(authorResourceParameters));

            // sino completan la categoria querran obtener todos los Autores y sino completan el searchQuery no querrán buscar algo y retorno todos los Autores
            if (string.IsNullOrEmpty(authorResourceParameters.MainCategory) && string.IsNullOrEmpty(authorResourceParameters.SearchQuery))
                return GetAllAuthors();

            // Directiva para ir construyendo la consulta
            var collection = _context.Authors as IQueryable<Author>;

            // Maincategory -> Armando query abajo
            if(!string.IsNullOrEmpty(authorResourceParameters.MainCategory))
            {
                // Formateo de espacios en blanco
                authorResourceParameters.MainCategory = authorResourceParameters.MainCategory.Trim();

                collection = collection
                    .Where(a => a.MainCategory == authorResourceParameters.MainCategory);
            }

            // SEARCH QUERY -> Armando query abajo
            if (!string.IsNullOrEmpty(authorResourceParameters.SearchQuery))
            {
                authorResourceParameters.SearchQuery = authorResourceParameters.SearchQuery.Trim();

                collection = collection
                    .Where(a => a.MainCategory.Contains(authorResourceParameters.SearchQuery)
                    || a.Firstname.Contains(authorResourceParameters.SearchQuery)
                    || a.Lastname.Contains(authorResourceParameters.SearchQuery));
            }

            // Aqui recien se ejecuta la query
            return collection.ToList(); 
        }
        #endregion
    }
}
