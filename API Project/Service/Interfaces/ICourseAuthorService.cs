﻿using API_Project.ResourceParameters;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Interfaces
{
    public interface ICourseAuthorService
    {
        IEnumerable<Course> GetAllCourses(int authorId);
        Course GetCourse(int authorId, int courseId);
        void AddCourse(int authorId, Course course);
        void UpdateCourse(Course course);
        void DeleteCourse(Course course);



        //void DeleteCourseLogic();
        IEnumerable<Author> GetAllAuthors();
        Author GetAuthor(int id);
        IEnumerable<Author> GetAuthorsByCourse(IEnumerable<int> authorIds); //Autores por por cursos
        void AddAuthor(Author author);
        void UpdateAuthor(Author author);
        void DeleteAuthor(Author author);
        void DeleteAuthorLogic(int authorId);
        void UpdateJustFirstname(int authorId, string newName);
        bool AuthorExists(int authorId);


        // Búsqueda y Filtros
        IEnumerable<Author> GetAllAuthors(AuthorsResourceParameters authorResourceParameters);
    }
}
